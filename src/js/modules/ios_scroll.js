var mob = 992; //width for hidding top nav
var lastWindowWidth = window.innerWidth;
var actual_scroll; //current scroll from top position
var is_ready = false; //is page loaded

window.addEventListener("touchmove", Scroll);
window.addEventListener("scroll", Scroll);

/* Define is device touchable */
function is_touch_device() {
	return 'ontouchstart' in window || navigator.maxTouchPoints
}


/* Define is device on iOS */
function is_iOS() {
	var iDevices = [
		'iPad Simulator',
		'iPhone Simulator',
		'iPod Simulator',
		'iPad',
		'iPhone',
		'iPod'
	];
	while (iDevices.length) {
		if (navigator.platform === iDevices.pop()) {
			return true
		}
	}
	return false
}


window.onload = function () {
	is_ready = true;
	Scroll()
}


$(document).ready(function () {
	if (is_iOS() && is_touch_device() && window.innerWidth < 1000) {
		$("html").addClass("nm-ios");
		$(".nm-main-overlay").css("cursor", "pointer")
	}
})

function Scroll() {
	var scrollPos = $(document).scrollTop();
}

//menu toggle
$(".header__expand--open").on("click", function (e) {
	e.preventDefault();

	actual_scroll = $(window).scrollTop();
	$("body,html").addClass("fixed");
	if (window.innerWidth <= mob) {
		$('body').addClass('header__active');
		$("body").scrollTop(actual_scroll)
	}
})


//aside close button
$(".header__expand--close").on("click", function (e) {
	e.preventDefault();

	$("body,html").removeClass("fixed");
	if (window.innerWidth <= mob) {
		$('body').removeClass('header__active');
		$(window).scrollTop(actual_scroll)
	}
})