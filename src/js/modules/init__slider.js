var maxWidth = 630;

$(function(){


	// slick init
	$('.slider-js').slick({
		rtl: true,
		arrows: true,
		slidesToShow: 4,
		infinite: true,
		responsive: [
			{
				breakpoint: 998,
				settings: {
					slidesToShow: 2,
					spaceBetween: 0,
					dots: false
				}
			},
			{
				breakpoint: maxWidth,
				settings: 'unslick'
			}
		]
	});
	$('.guides-slider-js').slick({
		rtl: true,
		arrows: true,
		dots: false,
		slidesToShow: 3,
		infinite: true,
		responsive: [
			{
				breakpoint: 998,
				settings: {
					slidesToShow: 2,
					spaceBetween: 0,
					dots: false
				}
			},
			{
				breakpoint: maxWidth,
				settings: 'unslick'
			}
		]
	});

	$('.js-aside-slider').slick({
		rtl: true,
		arrows: true,
		dots: false,
		slidesToShow: 1,
		infinite: true
	});

	var bannerSlider = $('.js-banner-slider').slick({
		rtl: true,
		arrows: false,
		dots: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		slidesPerRow: 1,
		rows: 0,
		infinite: true,
		dotsClass: "banner-slider__pagination"
	});

	$('.gallery_slider-js').slick({
		rtl: true,
		arrows: false,
		dots: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		slidesPerRow: 1,
		rows: 0,
		infinite: true,
		dotsClass: "banner-slider__pagination"
	});


})
$(window).on('resize orientationChange', function(){
	var width = $(window).width();
	if(width > maxWidth) {
		$('.slider-js').slick({
			rtl: true,
			arrows: true,
			slidesToShow: 4,
			infinite: true,
			responsive: [
				{
					breakpoint: 998,
					settings: {
						slidesToShow: 2,
						spaceBetween: 0,
						dots: false
					}
				},
				{
					breakpoint: maxWidth,
					settings: 'unslick'
				}
			]
		});
		$('.guides-slider-js').slick({
			rtl: true,
			arrows: true,
			dots: false,
			slidesToShow: 3,
			infinite: true,
			responsive: [
				{
					breakpoint: 998,
					settings: {
						slidesToShow: 2,
						spaceBetween: 0,
						dots: false
					}
				},
				{
					breakpoint: maxWidth,
					settings: 'unslick'
				}
			]
		});
	}
});